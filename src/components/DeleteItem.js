import UseRequest from '../hooks/useRequest';

const DeleteItem = ({ handleItemDelete, itemId }) => {
  const deleteItemRequest = UseRequest({
    url: `/api/items/${itemId}`,
    method: 'delete',
    body: {},
    onSuccess: (response) => {
      handleItemDelete(itemId);
    },
  });

  const onSubmit = (e) => {
    e.preventDefault();
    deleteItemRequest();
  };

  return (
    <div className="delete-item">
      <form onSubmit={onSubmit}>
        <button id="delete-button">Delete</button>
      </form>
    </div>
  );
};
export default DeleteItem;
