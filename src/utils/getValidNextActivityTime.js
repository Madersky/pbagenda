const getValidNextActivityTime = (
  lastActivity,
  endOfTrainingDay
) => {
  const quants = [60, 30, 15, 10, 5];
  let strTime = '';

  for (const quant of quants) {
    const newDate = addMinutes(
      new Date(lastActivity.activityTime),
      quant
    );

    if (newDate < new Date(endOfTrainingDay.activityTime)) {
      let [hours, minutes] = [
        newDate.getHours(),
        newDate.getMinutes(),
      ];

      hours = hours < 10 ? '0' + hours : hours;
      minutes = Math.ceil(minutes / 5) * 5;
      minutes = minutes < 10 ? '0' + minutes : minutes;

      strTime = hours + ':' + minutes;

      break;
    }
  }
  return strTime;
};

const addMinutes = (date, minutes) => {
  return new Date(date.getTime() + minutes * 60000);
};

export default getValidNextActivityTime;
