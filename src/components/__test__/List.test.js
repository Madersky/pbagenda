import React from 'react';
import axios from 'axios';

import '@testing-library/jest-dom/extend-expect';
import { render } from '@testing-library/react';

import '../../mocks/server';

import List from '../List';

test('renders component', async () => {
  const response = await axios('/api/items');

  const { getByText } = render(
    <List items={response.data.items} />
  );

  expect(
    getByText('Start of Trening Day')
  ).toBeInTheDocument();
});
