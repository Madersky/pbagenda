const dateParser = (date) => {
  var hours = date.getHours();
  var minutes = date.getMinutes();

  hours = hours < 10 ? '0' + hours : hours;
  minutes = minutes < 10 ? '0' + minutes : minutes;

  var strTime = hours + ':' + minutes;

  return strTime;
};
export default dateParser;
