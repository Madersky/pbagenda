import { useState } from 'react';
import UseRequest from '../hooks/useRequest';

import dateParser from '../utils/dateParser';
import getValidNextActivityTime from '../utils/getValidNextActivityTime';
import createDate from '../utils/createDate';

const AddItem = ({
  handleItemAdd,
  lastActivity,
  endOfTrainingDay,
}) => {
  const [activity, setActivity] = useState('');
  const [description, setDescription] = useState('');
  const [activityTime, setActivityTime] = useState(null);
  const [isOpen, setIsOpen] = useState(false);

  const addItemRequest = UseRequest({
    url: '/api/items/new',
    method: 'post',
    body: {
      activity,
      description,
      activityTime,
    },
    onSuccess: (response) => {
      handleItemAdd(response.item);
      setActivityTime(null);
      setDescription('');
      setActivity('');
    },
  });

  const getValidTime = async (strTime) => {
    const validTime = await createDate(strTime);
    setActivityTime(validTime);
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    if (
      !activityTime instanceof Date ||
      isNaN(activityTime)
    ) {
      window.alert('Brak daty!');
    } else if (
      activityTime <= new Date(lastActivity.activityTime)
    ) {
      window.alert(
        'Nie mozna dodać pozycji agendy wcześniej niż ostatnia pozycja agendy'
      );
    } else if (
      activityTime >=
      new Date(endOfTrainingDay.activityTime)
    ) {
      window.alert(
        'Próbujesz dodać pozycję agendy poza ramami czasowymi treningu'
      );
    } else {
      addItemRequest();
    }
  };

  return (
    <div className="add-item">
      <button
        className="add-item__switcher"
        onClick={() => setIsOpen(!isOpen)}
      >
        {isOpen === true ? 'Close' : 'Add Activity'}
      </button>
      {isOpen === true ? (
        <form
          className="add-item__form"
          onSubmit={onSubmit}
        >
          <input
            type="text"
            value={activity}
            onChange={(e) => setActivity(e.target.value)}
            placeholder="Activity"
          />
          <input
            type="text"
            value={description}
            onChange={(e) => setDescription(e.target.value)}
            placeholder="Description"
          />

          <input
            type="time"
            value={
              activityTime
                ? dateParser(activityTime)
                : getValidNextActivityTime(
                    lastActivity,
                    endOfTrainingDay
                  )
            }
            onChange={(e) => getValidTime(e.target.value)}
          />
          <button
            disabled={!activity ? true : false}
            value={
              activityTime
                ? `${activityTime.getHours()}:${activityTime.getMinutes()}`
                : getValidNextActivityTime(
                    lastActivity,
                    endOfTrainingDay
                  )
            }
            onClick={(e) => getValidTime(e.target.value)}
          >
            Submit
          </button>
        </form>
      ) : (
        ''
      )}
    </div>
  );
};
export default AddItem;
