const createDate = (providedTime) => {
  const today = new Date();

  const [year, month, day] = [
    today.getFullYear(),
    today.getMonth(),
    today.getDate(),
  ];

  let [hours, minutes] = providedTime.split(':');

  minutes = Math.ceil(parseInt(minutes) / 5) * 5;

  const activityTime = new Date(
    year,
    month,
    day,
    parseInt(hours),
    minutes
  );

  return activityTime;
};

export default createDate;
