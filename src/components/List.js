import React from 'react';

import AgendaPositionTypes from '../const/agendaPositionTypes';

import Item from './Item';

import DeleteItem from './DeleteItem';

const List = ({ items, handleItemDelete }) => {
  return (
    <div className="list">
      {items.map((item) => {
        return (
          <div className="list__item" key={item.id}>
            <Item item={item} />
            {item.type === AgendaPositionTypes.TIMEFRAME ? (
              ''
            ) : (
              <DeleteItem
                itemId={item.id}
                handleItemDelete={handleItemDelete}
              />
            )}
          </div>
        );
      })}
    </div>
  );
};

export default List;
