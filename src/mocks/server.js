import { createServer, Model, Response } from 'miragejs';

import AgendaPositionTypes from '../const/agendaPositionTypes';
createServer({
  models: {
    item: Model,
  },

  routes() {
    this.namespace = 'api';

    this.get('/items', async () => {
      return new Response(
        200,
        { headers: 'headers' },
        this.schema.items.all()
      );
    });

    this.post('/items/new', (schema, request) => {
      let attrs = JSON.parse(request.requestBody);

      return schema.items.create(attrs);
    });

    this.delete('/items/:id', (schema, request) => {
      const params = request.params;

      return schema.items.find(params.id).destroy();
    });
  },

  //Do poprawnego działania aplikacji trzeba zmienić itemach property activityTime,
  //a konkretniej przestawić dzień na "dzisiejszy"

  seeds(server) {
    server.create('item', {
      activity: 'Start of Trening Day',
      type: AgendaPositionTypes.TIMEFRAME,
      activityTime: new Date('December 17, 2021 09:00:00'),
    });
    server.create('item', {
      activity: 'Training 1',
      type: AgendaPositionTypes.ACTIVITY,
      activityTime: new Date('December 17, 2021 09:30:00'),
    });
    server.create('item', {
      activity: 'Dinner break',
      type: AgendaPositionTypes.ACTIVITY,
      description: 'Now is the time!',
      activityTime: new Date('December 17, 2021 11:00:00'),
    });
    server.create('item', {
      activity: 'Dinner break 2',
      type: AgendaPositionTypes.ACTIVITY,
      description: 'Now is the time!',
      activityTime: new Date('December 17, 2021 17:00:00'),
    });
    server.create('item', {
      activity: 'Training 2',
      type: AgendaPositionTypes.ACTIVITY,
      activityTime: new Date('December 17, 2021 17:15:00'),
    });
    server.create('item', {
      activity: 'End of Trening Day',
      type: AgendaPositionTypes.TIMEFRAME,
      activityTime: new Date('December 17, 2021 23:00:00'),
    });
  },
});
