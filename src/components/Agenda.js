import { useState, useEffect } from 'react';
import UseRequest from '../hooks/useRequest';

import '../styles/global.scss'

import AddItem from './AddItem';
import List from './List';

import sortItems from '../utils/sortItems';

const Agenda = () => {
  const [items, setItems] = useState([]);

  const getItemsRequest = UseRequest({
    url: '/api/items',
    method: 'get',
    onSuccess: (response) => {
      const sortedItems = sortItems(response.items);
      setItems(sortedItems);
    },
  });

  useEffect(() => {
    getItemsRequest();
  }, [getItemsRequest]);

  const handleItemAdd = (newItem) => {
    const allItems = [...items, newItem];
    const sortedItems = sortItems(allItems);
    setItems(sortedItems);
  };
  const handleItemDelete = (delItemId) => {
    setItems(items.filter((item) => item.id !== delItemId));
  };

  return (
    <div className="agenda">
      <AddItem
        handleItemAdd={handleItemAdd}
        lastActivity={items[items.length - 2]}
        endOfTrainingDay={items[items.length - 1]}
      />
      <List
        items={items}
        handleItemDelete={handleItemDelete}
      />
    </div>
  );
};

export default Agenda;
