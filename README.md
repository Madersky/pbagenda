To run application type:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### Mockowanie

Mockowanie zrobione jest bardzo bardzo prosto, nie wiem czy o taką formę chodziło, zrobiłem je tak jak potrafię i tak, żeby imitowało pracę normalną pracę z API. Rozwiązanie to jest mi całkiem bliskie (chociaż pierwszy raz używałem biblioteki Mirage.js), ponieważ wcześniej tworzyłem API w expressie.

### Do poprawnego działania aplikacji w itemach "populatowanych" przez serwer trzeba zmienić property activityTime, a konkretniej przestawić dzień na "dzisiejszy"

Założyłem fakt, że `Start i End of Training Day` są domyślnie tworzone na serwerze i wysyłane do klienta, ponieważ w instrukcji zadania napisane jest `agenda składa się z od 2 (wymagane pozycje patrz ponizej) do n > 2 pozycji agendy` ponadto `agenda zawsze ma następujące pozycje: "Start of Trening Day" i "End of Training Day", które wyznaczają ramy czasowe treningu ` , a więc klient nie może tworzyć tych dwóch pozycji w agendzie (chyba, że totalnie nie zrozumiałem wytycznych)

### Aplikacja - Wygląd

Wygląd jest, tak jak wspomniane w wytycznych "zwykły/prosty", do jego utworzenia wykorzystywałem pliki SCSS i metodologię BEM, uważam ją za bardzo czytelną i wygodną w użyciu.

### Aplikacja - Komponenty

W aplikacji starałem się zachowywać komponenty jak najmniejsze, by nie przeładowywać ich logiką.

### Aplikacja - Funkcjonalności

`dodawanie pozycji agendy`

Do uzupełnienia posiadamy 3 pola - activity tj. nazwa aktywności - description tj. opcjonalne pole opisu aktywności - activityTime tj. czas wykonania aktywności

Po uzupełnieniu wymaganych pól, możliwe jest dodanie pozycji agendy, która dodawana jest do bazy danych(mock).

`usuwanie pozycji agendy`

Usuwamy pozycję agendy klikając button "Delete", przycisk ten pojawia się tylko przy elementach agendy posiadających property "type" z wartością "activity". po kliknięciu item usuwany jest z bazy danych(mock)

`wyświetlanie wszystkich pozycji agendy`

Wyświetlamy agendę po otrzymaniu response z serwera(mock), a następnie posortowaniu aktywności po polu activityTime.

`Wyświetlanie domyślnej godziny startu`

Input w którym wybieramy godzinę startu aktywności treningowej domyślnie ustawia nam wartość tego pola na taką która jest dostępna jako różnica czasowa od ostatniego treningu do końca dnia treningowego tj. czas ostatniej aktywności + kwant dostępnego czasu przed końcem dnia treningu (nie więcej niż 1h i nie mniej niż 5 minut)

`minimalny kwant czasu 5min`

W zadaniu spotkałem się z informacją, `minimalny kwant czasu to 5 min`, zatem zaimplementowałem taką funkcjonalność. Ustawiając czas rozpoczęcia aktywności na `19:03` aplikacja ustawi `19:05`.

### Aplikacja - Restrykcje

Zgodnie z moim zrozumieniem zadania w aplikacji zostały zaimplementowane następujące restrykcje:

`Nie można utworzyć aktywności przed rozpoczęciem dnia treningowego`
`Nie można utworzyć aktywności przed ostatnią utworzoną aktywnością`
`Nie można utworzyć aktywności w takim samym czasie jak inna aktywność`
`Nie można utworzyć aktywnośći po czasie dnia treningowego `

### Testy

Niestety, nie posiadam wystarczającej wiedzy w przeprowadzaniu testów aplikacji Reactowych, napisałem jeden test, banalny, sprawdzający czy tabela się wyświetla poprawnie tj. posiada dane z serwera.

### Podsumowanie

Mam nadzieję, że zaprezentowane przeze mnie rozwiązanie jest poprawne, zdaję sobie sprawę, że zadanie to można by było wykonać dużo lepiej, znając różnego rodzaju wzorce budowy aplikacji poparte nie tylko wiedzą teoretyczną ale i `praktyczną`. Wiem, że w odpowiednim środowisku, mogę rozwinąć się `szybko` i `wnosić wartość do firmy` jako programista.

Jeśli moje rozwiązanie nie będzie zadowalające, prosiłbym o feedback, który opisałby co zrobiłem źle i w jaki sposób powinienem był to zrobić, pomoże mi to stać się lepszym programistą i nie popełniać na przyszość tych błędów.

### Informacje dodatkowe

Zadanie zajęło mi około `16 godzin`
