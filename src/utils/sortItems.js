const sortItems = (items) => {
  const sortedItems = items.sort((a, b) =>
    a.activityTime < b.activityTime ? -1 : 1
  );

  return sortedItems;
};

export default sortItems;
