const AgendaPositionTypes = Object.freeze({
  TIMEFRAME: 'timeframe',
  ACTIVITY: 'activity',
});

export default AgendaPositionTypes;
