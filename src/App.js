import React from 'react';

import Agenda from './components/Agenda';

import './mocks/server';

const App = () => {
  return (
    <div className="container">
      <Agenda />
    </div>
  );
};

export default App;
