import React from 'react';

import dateParser from '../utils/dateParser';
import '../mocks/server';

const Item = ({ item }) => {
  return (
    <div className="item">
      <div className="item__info">
        <p>{item.activity}</p>
        <p>{dateParser(new Date(item.activityTime))}</p>
      </div>
      {item.description ? <p>{item.description}</p> : ''}
    </div>
  );
};

export default Item;
